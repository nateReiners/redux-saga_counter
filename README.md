# redux-saga-beginner-tutorial
This is the resulting code after following the [Redux/Redux-saga beginner tutorial](https://github.com/redux-saga/redux-saga/blob/master/docs/introduction/BeginnerTutorial.md)

# Testing Instructions

// clone the repo
git clone git@gitlab.com:nateReiners/redux-saga_counter.git

cd redux-saga-beginner-tutorial

npm install
```

Run the demo

```
npm start
```

Run tests

```
npm test
```
